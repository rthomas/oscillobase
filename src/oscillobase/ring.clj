(ns oscillobase.ring
  (:use compojure.core)
  (:require [oscillobase.core :as osc])
  (:require [compojure.route :as route])
  (:require [ring.middleware.params :as rmp])
  (:require [clojure.core.async :refer :all]))

(defn add-to-index [id state]
  (osc/add id state)
  {:status 204})

(defn queryid [id count]
  (let [results (osc/queryid id count)]
    (if (empty? results)
      {:status 404}
      {:status 200 :body results})))

(defn query-state
  ([id] (query-state id (System/currentTimeMillis)))
  ([id ts]
  (if-let [result (first (osc/queryid id 0 ts 1))]
    {:status 200 :body (list result)}
    {:status 404})))

(defroutes rts
;  (GET "/a" [z & more] (pr-str z more))
  (route/resources "/")
  (PUT "/:id/:state" [id state] (add-to-index id state))
  (GET "/:id" [id count] (queryid id (Integer. (or count 10))))
  (GET "/:id/latest" [id] (query-state id))
  (GET "/:id/:ts" [id ts] (query-state id ts))
  )

(def app (rmp/wrap-params rts))