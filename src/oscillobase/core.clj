(ns oscillobase.core
  (:require [clucy.core :as clucy]))

;(def index (clucy/memory-index))
(def index (clucy/disk-index "/tmp/oscillobase.idx"))

(clucy/add index {:init (System/currentTimeMillis)})

(def index-agent (agent index))

(defn queryid
  ([id max-results]
    (queryid id 0 (System/currentTimeMillis) max-results))
  ([id ^Long from-ts ^Long to-ts max-results]
    (clucy/search index (str "id:" id " AND ts:[" from-ts " TO " to-ts "]") max-results :sort-by "ts" :sort-type "LONG" :reverse true)))

(defn- addvalue [idx id state]
  (let [now (System/currentTimeMillis) latest (first (queryid id 0 now 1))]
    (when (or (nil? latest) (not (= state (:state latest))))
      (clucy/add idx {:id id :state state :ts now :lastts now})))
  idx)

(defn add [id state]
  (send-off index-agent addvalue id state))