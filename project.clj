(defproject oscillobase "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [clucy "0.4.0-rthomas-1-SNAPSHOT"]
                 [compojure "1.1.5"]
                 [core.async "0.1.0-SNAPSHOT"]]
  :plugins [[lein-ring "0.7.1"]]
  :ring {:handler oscillobase.ring/app})
