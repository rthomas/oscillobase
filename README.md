NOTE
Requires this version of clucy until the PR is merged.
REPO:  https://github.com/rthomas/clucy
PR:    https://github.com/weavejester/clucy/pull/18

Running:
$ lein ring server